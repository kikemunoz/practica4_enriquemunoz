package clases;

import java.time.LocalDate;

public class Alumno {

	private String codAlumno;
	private String nombre;
	private String apellidos;
	private String codAula;
	private double nota;
	private LocalDate fechaMatricula;
	
	public Alumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public Alumno() {
		
	}

	public String getCodAlumno() {
		return codAlumno;
	}

	public void setCodAlumno(String codAlumno) {
		this.codAlumno = codAlumno;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCodAula() {
		return codAula;
	}

	public void setCodAula(String codAula) {
		this.codAula = codAula;
	}

	public double getNota() {
		return nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	public LocalDate getFechaMatricula() {
		return fechaMatricula;
	}

	public void setFechaMatricula(LocalDate fechaMatricula) {
		this.fechaMatricula = fechaMatricula;
	}

	@Override
	public String toString() {
		return "Alumno [codAlumno=" + codAlumno + ", nombre=" + nombre + ", apellidos=" + apellidos + ", codAula="
				+ codAula + ", nota=" + nota + ", fechaMatricula=" + fechaMatricula + "]";
	}
	
	
}
