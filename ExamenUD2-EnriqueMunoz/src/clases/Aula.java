package clases;

public class Aula {

	Alumno[] alumnos;

	public Aula(int maxAlumno) {
		this.alumnos = new Alumno[maxAlumno];
	}

	public void altaAlumno(String codAlumno, String nombre, String apellidos, String codAula, double nota) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				alumnos[i] = new Alumno();
				alumnos[i].setCodAlumno(codAlumno);
				alumnos[i].setNombre(nombre);
				alumnos[i].setApellidos(apellidos);
				alumnos[i].setCodAula(codAula);
				alumnos[i].setNota(nota);

			}
		}

	}

	public Alumno buscarAlumno(String codAlumno) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					System.out.println("Alumno encontrado: " + alumnos[i]);
				}
			}
		}
		return null;
	}

	public void eliminarAlumno(String codAlumno) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					alumnos[i] = null;
				}
			}
		}
	}

	public void listarAlumnos() {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				System.out.println(alumnos[i].toString());
			}
		}
	}

	public void cambiarNombreAlumno(String codAlumno, String nombre2) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAlumno().equals(codAlumno)) {
					alumnos[i].setNombre(nombre2);
				}
			}
		}

	}

	public void listarAlumnoPorAula(String codAula) {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getCodAula().equals(codAula))
					System.out.println(alumnos[i].toString());
			}
		}
	}

	public void contarAlumnosAprobados() {
		for (int i = 0; i < alumnos.length; i++) {
			if (alumnos[i] != null) {
				if (alumnos[i].getNota() > 5) {
					System.out.println(alumnos[i]);
				}
			}
		}

	}
}
