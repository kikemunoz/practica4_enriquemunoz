package programa;

import clases.Aula;

public class Programa {

	public static void main(String[] args) {
		int maxAlumnos = 4;
		System.out.println("1.- Creo una instancia de Aula llamada miAula con 4 alumnos");
		Aula miAula = new Aula(maxAlumnos);

		System.out.println("2.- Dar de alta 4 alumnos");
		miAula.altaAlumno("233", "Pepe", "Rodriguez", "65", 7);
		miAula.altaAlumno("243", "Sara", "Domingo", "34", 4);
		miAula.altaAlumno("253", "Fede", "Mu�oz", "34", 7);
		miAula.altaAlumno("263", "Elena", "Fuentes", "34", 10);
		
		System.out.println("3.- Lista de alumnos");
		miAula.listarAlumnos();

		System.out.println("4.- Buscar un alumno por su c�digo");
		miAula.buscarAlumno("233");
		
		System.out.println("5.- Eliminar un alumno");
		miAula.eliminarAlumno("243");
		
		System.out.println("6.- Almacenar un nuevo alumno");
		miAula.altaAlumno("273", "Carlos", "Perez", "12", 5);
		
		System.out.println("7.- Modificar el nombre de un alumno");
		miAula.cambiarNombreAlumno("263", "Pedro");
		
		System.out.println("8.- Listar solo los alumnos de un aula");
		miAula.listarAlumnoPorAula("34");
		
		System.out.println("9.- Mostrar cuantos alumnos aprobados tengo en el aula");
		miAula.contarAlumnosAprobados();
		
		
	}

}
