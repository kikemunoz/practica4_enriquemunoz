package clases;

import java.util.Scanner;

//Clase que actua de almacen de artistas

public class Album {
	static Scanner input = new Scanner(System.in);

	// Atributo vector de artistas
	Artista[] artistas;

	// Constructor
	public Album(int maxArtistas) {
		this.artistas = new Artista[maxArtistas];
	}

	// 1 M�todo para dar de alta un artista
	public void altArtista() {
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] == null) {
				artistas[i] = new Artista();
				System.out.println("Introduce el nombre:");
				String nombre = input.nextLine();
				artistas[i].setNombre(nombre);
				System.out.println("Introduce el pa�s:");
				String pais = input.nextLine();
				artistas[i].setPais(pais);
				System.out.println("Introduce la edad:");
				int edad = input.nextInt();
				artistas[i].setEdad(edad);
				input.nextLine();
				System.out.println("Introduce el g�nero m�sical:");
				String generoMusical = input.nextLine();
				artistas[i].setGeneroMusical(generoMusical);

				break;
			}
		}
	}

	// 2 M�todo para buscar un artista
	public void buscarArtista() {
		boolean encontrado = false;
		System.out.println("Introduce el nombre del artista que desea buscar:");
		String nombreArtista = input.nextLine();
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getNombre().equals(nombreArtista)) {
					System.out.println("Artista " + (i + 1) + artistas[i]);
					encontrado = true;
				}
			}
		}
		if (!encontrado) {
			System.out.println("Nombre del artista no encontrado.");
		}

	}

	// 3 M�todo para eliminar un artista
	public void eliminarArtista() {
		System.out.println("Introduce el nombre del artista que desea eliminar:");
		String nombreArtista = input.nextLine();
		boolean encontrado = false;
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getNombre().equals(nombreArtista)) {
					artistas[i] = null;
					encontrado = true;
				}
			}

		}
		if (encontrado) {
			System.out.println("Artista eliminado.");
		} else {
			System.out.println("Nombre del artista no encontrado.");
		}

	}

	// 4 M�todo para listar artistas
	public void listarArtistas() {
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				System.out.println("Artista " + (i + 1) + artistas[i]);
			}
		}
	}

	// 5 M�todo para cambiar la edad de un artista
	public void cambiarEdadArtista() {
		boolean encontrado = false;
		System.out.println("Introduce el nombre del artista:");
		String nombreArtista = input.nextLine();
		System.out.println("Introduce la edad:");
		int edadArtista = input.nextInt();
		input.nextLine();
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getNombre().equals(nombreArtista)) {
					artistas[i].setEdad(edadArtista);
					encontrado = true;
				}
			}
		}
		if (encontrado) {
			System.out.println("Edad del artista modificada.");
		} else {
			System.out.println("Nombre del artista no encontrado.");
		}

	}

	// 6 M�todo para listar artistas por g�nero
	public void listarArtistaPorGenero() {
		boolean encontrado = false;
		System.out.println("Introduce el g�nero musical:");
		String genero = input.nextLine();
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getGeneroMusical().equals(genero)) {
					System.out.println("Artista " + (i + 1) + artistas[i]);
					encontrado = true;
				}
			}
		}
		if (!encontrado) {
			System.out.println("G�nero m�sical no encontrado.");
		}

	}

	// 7 M�todo para buscar artistas que comiencen por una letra concreta
	public void buscarArtistaPorCadena() {
		boolean encontrado = false;
		System.out.println("Introduce la inicial por la que empieza el nombre del artista que desea buscar:");
		String cadenaArtista = input.nextLine();
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getNombre().startsWith(cadenaArtista)) {
					System.out.println("Artista " + (i + 1) + artistas[i]);
					encontrado = true;
				}

			}
		}
		if (!encontrado) {
			System.out.println("No hay ning�n artista que empiece por esa cadena.");
		}
	}

	// 8 M�todo para comprobar si un artista ha sido dado de alta
	public void comprobarArtistaDadoAlta() {
		boolean encontrado = false;
		System.out.println("Introduce el nombre del artista que desea comprobar:");
		String nombreArtista = input.nextLine();
		for (int i = 0; i < artistas.length; i++) {
			if (artistas[i] != null) {
				if (artistas[i].getNombre().equals(nombreArtista)) {
					System.out.println("El artista " + (i + 1) + " ha sido de alta con ese nombre.");
					encontrado = true;
				}
			}
		}
		if (!encontrado) {
			System.out.println("No ha sido dado de alta ning�n artista con ese nombre");
		}

	}

	// 9 M�todo para listar artistas en orden inverso
	public void listarArtistasOrdenInverso(Album discografica) {

		Artista[] artistasInvertido = new Artista[artistas.length];

		for (int i = (artistas.length - 1), j = 0; i >= 0; i--, j++) {
			if (artistas[i] != null) {
				artistasInvertido[j] = artistas[i];
				System.out.println("Artista " + (i + 1) + artistasInvertido[j]);
			}
		}
	}

}
