package clases;

//Clase en la que se guarda información de distintos artistas

public class Artista {
	//Atributos
	String nombre;
	String pais;
	int edad;
	String generoMusical;
	
	//Constructor	
	public Artista() {
		this.nombre = "";
		this.pais = "";
		this.edad = 0;
		this.generoMusical = "";
	}

	//Setter y getter
	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getPais() {
		return pais;
	}


	public void setPais(String pais) {
		this.pais = pais;
	}


	public int getEdad() {
		return edad;
	}


	public void setEdad(int edad) {
		this.edad = edad;
	}


	public String getGeneroMusical() {
		return generoMusical;
	}


	public void setGeneroMusical(String generoMusical) {
		this.generoMusical = generoMusical;
	}

	//Método toString
	@Override
	public String toString() {
		return "\n" +"Nombre: " + nombre + "\n" +"Pais: " + pais + "\n" +"Edad: " + edad + "\n" +"Genero Musical: " + generoMusical+ "\n";
	}
	
	
	
}
