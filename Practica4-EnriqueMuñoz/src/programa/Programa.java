package programa;

import java.util.Scanner;

import clases.Album;

public class Programa {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		// declaro maxArtistas
		int maxArtistas = 15;
		// Creo una instancia de album llamada discografica
		Album discografica = new Album(maxArtistas);

		int opcion;

		do {
			System.out.println("");
			System.out.println("__________________ Practica 4 ________________");
			System.out.println("");
			System.out.println("Tem�tica: M�sica                 Enrique Mu�oz");
			System.out.println("______________________________________________");
			System.out.println("");
			System.out.println("                �Qu� desea hacer?             ");
			System.out.println("");
			System.out.println("1. - Dar de alta un artista");
			System.out.println("2. - Buscar un artista");
			System.out.println("3. - Eliminar un artista");
			System.out.println("4. - Listar artistas");
			System.out.println("5. - Cambiar la edad artista");
			System.out.println("6. - Listar artistas por g�nero m�sical");
			System.out.println("7. - Buscar artista por inicial de su nombre");
			System.out.println("8. - Comprobar si artista ha sido dado de alta");
			System.out.println("9. - Listar artistas por orden de �ltima alta");
			System.out.println("10.- Salir");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				discografica.altArtista();
				break;
			case 2:
				discografica.buscarArtista();
				break;
			case 3:
				discografica.eliminarArtista();
				break;
			case 4:
				discografica.listarArtistas();
				break;
			case 5:
				discografica.cambiarEdadArtista();
				break;
			case 6:
				discografica.listarArtistaPorGenero();
				break;
			case 7:
				discografica.buscarArtistaPorCadena();
				break;
			case 8:
				discografica.comprobarArtistaDadoAlta();
				break;
			case 9:
				discografica.listarArtistasOrdenInverso(discografica);
				break;
			case 10:
				System.out.println("Fin del programa.");
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n no v�lida, vuelva a intentarlo.");
			}

		} while (opcion != 10);

		input.close();

	}

}